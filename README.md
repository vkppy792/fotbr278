#第一财经集结号上下分比例ihp
#### 介绍
集结号上下分比例【溦:5546055】，集结号游戏上下分客服微信多少【溦:5546055】，集结号游戏上分下分商家微信号是多少【溦:5546055】，集结号充值上下分微信【溦:5546055】，集结号游戏上下分商人【溦:5546055】，有人说，有些人分开后，就一定要删除拉黑永不见。但今天我在朋友圈看到一句话：
/>他突然消失了王梅芳　　冬天的早晨6点，天还黑乎乎的，一弯残月挂在西天，冷风吹得人脸发麻。我骑车出门买早点，看见他端着小铁锅，正要横穿马路去买粥。他的脸色灰暗，神情有些恍惚，我喊住他，一边买油条一边嘱咐他中午别忘了给接孩子，他连连答应着，又像往常一样从兜里往外掏钱，我忙说我有零钱，没好气地把钱推给他，把他的唠叨甩在身后，头也不回地急急往家赶。没想到2分钟以后，他就被一辆快速行驶的汽车撞出16米之远，死在家门前那条新拓宽的水泥路上。洒在地上的粥还冒着热气，鲜艳的血迹很快结了冰。那条短短的矿区路，他走了50年，熟悉得不能再熟悉了。　　我总是在想，如果我不喊住他，如果我要过小锅替他把粥买来，或者我多听他絮叨几句，是不是就能躲过那场灾难呢？　　他是我的公公，11年的亲情相处，他疼我如己出，我却怎么也不喜欢他，更谈不上尽点孝心了。他是个一丝不苟的会计师，管了半辈子的帐没出现过一分钱的差错，秉性刚烈耿直，文革中挨斗后，头脑受了刺激，脾气变得不好，嗓门特大特高，什么事都爱较个真，爱管闲事爱喝酒，几乎是走到哪里就和人争吵到哪里，和卖菜的吵，和老邻居也吵，吵得人家都烦他，叫他“酒邪子”，我们则又气又好笑地喊他“战争犯子”。几乎没有人喜欢他，懒得答理他，他的儿子们凶凶地叱责他，任他可怜巴巴地央求，他百般疼爱的小孙子也不喊他一声爷爷。他很孤独，也很寂寞。但他又是一个那么善良的人，多年来老母鸡一样张着翅翼极尽所能地护住他的亲人们，不求半点回报。在纷纭的漫长的苦难里，在一片肃杀的冷漠里，他始终如一地保持了那份纯洁的善良，这又使他天真如孩童，任所有冷漠待他的人想起来就愧疚并为之黯然神伤。　　他的一生风雨飘摇穷愁潦倒，没过一天舒心日子。他的祖先曾经非常显赫过，到他父亲这辈，便穷得以卖河水为生了。他的父亲比母亲大22岁，他是长子，下边还有一个弟弟，他很小的时候就下井挖煤，用稚嫩的肩头挑起一个家庭的重担，尽全力供应弟弟读书，直到他北京大学毕业。他人长得英俊儒雅，又识文解字，家境富有的婆婆嫁给了他。婆婆比他大5岁，不识字，长得丑，火爆脾气，是个邋遢人，可还经常扼腕叹息：“我有一毛钱的文化也不跟你！”他的父母好吵架，后来相继瘫痪，他床前伺候了八年，是个有名的大孝子。他和婆婆合不来，磕磕碰碰了一辈子，吵到老时吵架便成了解脱寂寞的一种方式。堆满破烂的小单元里，两个白发人一个捏着酒盅儿，一个抽着自卷的喇叭烟，亮着嗓门直着脖子，针尖对麦芒地吵，吵得四邻不安儿嫌孙烦，更少人去了。半个字的、米粒大的一点小事，都能成为争执的导火索。　　在这样的环境里，他的三个儿子也都脾气暴躁性格反常。大儿子离婚再娶，扔给她一对不足两岁的男孩扬长而去。他含辛茹苦把孙子拉扯大，孙子却不争气，成了街头小混混儿，人高马大的天天伸手向他要钱。他的二儿子是我的丈夫，煤矿上的中层干部，年年劳模，是公公的精神寄托，可干得好好的突然就下岗了，呆在家里闲着，吃低保，公公嘴上不说什么，其实心里失落得很。他的三儿子看多了失败的婚姻，抱定独身的信念，三十大几仍一人在外漂泊，逢年过节也难见到他的影子。这都成了公公心头的重负。　　公公的离休金完全可以使他安度充裕的晚年，但他节俭惯了，对儿孙大方，对自己却吝啬得很，喝散酒拾旧衣服穿，挑最便宜的菜买，经常光顾垃圾堆，乱七八糟的什么都往家拣。拣来玩具娃娃，在她脖子上系上红头绳，说是拾来一个闺女。有一次还为我拣了一双棕色女式皮鞋，刷干净了要我穿。他拣垃圾是受婆婆的影响，婆婆跟着他穷怕了，破椅子烂坛子，什么都当成宝贝，日积月累，他家里便被垃圾塞满了，脏乱得一塌糊涂。还不让给动。　　公公在尘土飞扬的岁月里一直保持着一颗晶莹的童心。他的书法很好却只爱写对联，双手打起算盘来噼里啪啦如急风骤雨。高兴时拉拉二胡吹吹口琴，骑自行车赶集买两只绿莹莹的小蟋蟀或者一只廉价土气的小鸟、一盆草花，对他来说就很奢侈了。他在公厕后边种上葫芦，敲敲打打垒了一个鸡窝，年年养鸡，年年吃不上鸡，不是被黄鼠狼拖走了就是被小偷偷去了。有一年他喂的九只母鸡一只公鸡在一个夜里被人偷得净光，他挥毫泼墨写了个“寻鸡启事”，大红的纸贴在矿区墙上，引来一群又一群人，没一个人送还他的鸡。　　他出事的前几天，我看他实在寂寞，顺手抱了一摞旧杂志给他，要他无聊时一元一本卖卖看，晒晒太阳，不要老是喝酒。他认了真，用鞋架和废弃的旧纱窗做了书架，寒冬腊月一早就出摊，不到天黑不收，一天卖出三本，就高兴得什么似的，非把钱塞给我不可。我下班回来，看见他在肆虐的寒风里守着书架，脸都冻青了，心里发酸，后悔给他找了罪受，我像哄小孩子一样赶他回家，劝他不要再卖了。他跺跺脚说，也是，天太冷了，脚都冻伤了，等春天来了我再卖。哪知这话说过没一天，他就惨死在了毛头小子的车轮下，他再也等不到春天了。好长时间，我都不习惯街头没有了他的身影。他家门前是个小菜市场，他天天站在那儿玩，其实是想等我们路过时看上一眼。我带着孩子买什么，他就跟在后边付钱，不让付不行，并帮我讨价还价，恐怕我吃亏。有时我烦他，不让他跟在后边，冷着脸没好气地呵斥他回去，他也不恼，嘿嘿笑着问他的孙子还想要什么。十多年来，他每次见到我们的第一个动作是从兜里往外掏钱，孩子的爸爸下岗后，他掏得更勤了，看我们不要，就买好饭菜在路口久久地等着，见了我们高兴得哈哈笑，不由分说地就往车筐里塞。实在等不着就骑车送到我们家里去。他骑的车子是老式的大梁车，上下不方便，有一次我亲眼看见他狼狈地摔倒在地上，我冲上去要扶他，他尴尬地摆手不让。想着给他买辆轻便的，可什么样的他也用不着了。在四季更替的街头，他站成一棵树，站成一处恒久不变的风景。我从没想到他会突然地消失掉。冬天天短，我回来往往天已黑尽，北风呼啸的大路上，总见他缩着肩一个人默默地站在那儿，起初我以为是他饭后出来散步，打声招呼就从他身边匆匆骑车而过，连陪他说几句话的耐心都没有，留他一个人蹒跚着走回家。后来知道他是刻意在等我，等着我才放心地回去吃那豆汁泡煎饼的简单晚饭，心酸愧疚之余无限感动。但也仅仅是心酸感动而已，并未想着马上就要为他做点什么。他才72岁，身体还好，慢慢再孝敬不迟。他的眼神不好，怕看漏了，索性就站在我的三轮车旁边等，实在冻极了，才瑟缩着挪到墙根处。好几次我一下车就心急火燎地往家赶，没注意到缩在暗黑避风处的他，结果让他在寒夜里空等了两三个小时。他以为我的三轮车丢了，慌张地打电话询问，才知我早已把它骑回家，他释然地笑着说回来了好，回来了好，没有一点埋怨的口气。其实我回家正好走过他家的门前，我完全可以顺便过去一站，向他报个平安，但我没有那样做，他肯为我等上两三个小时，我却连两分钟的时间都不愿耽搁。他家里刚安上电话时，像孩子一样渴盼着接，电话一响，立刻磕磕绊绊地冲上去，一路带翻椅子碰倒杯子，激动得声音发抖。可大部分时间，他家的电话是沉寂着的。他家的门一天到晚虚掩着，一有风吹草动，他就两眼发光地问：“谁来啦？”谁也没有来。
https://vk.com/topic-225157884_50377776
https://vk.com/topic-225157404_50377783
https://vk.com/topic-225157884_50377789
https://vk.com/topic-225157404_50377796
https://vk.com/topic-225157884_50377801
https://vk.com/topic-225157404_50377808
https://vk.com/topic-225157884_50377809
https://vk.com/topic-225157404_50377819
https://vk.com/topic-225157884_50377818
https://vk.com/topic-225157404_50377832
https://vk.com/topic-225157884_50377828
https://vk.com/topic-225157884_50377839
https://vk.com/topic-225157404_50377851
https://vk.com/topic-225157884_50377850
https://vk.com/topic-225157404_50377862
https://vk.com/topic-225157884_50377863
https://vk.com/topic-225157404_50377873
https://vk.com/topic-225157884_50377874
https://vk.com/topic-225157884_50377887
https://vk.com/topic-225157404_50377889
https://vk.com/topic-225157884_50377897
https://vk.com/topic-225157404_50377898
https://vk.com/topic-225157884_50377907
https://vk.com/topic-225157884_50377917
https://vk.com/topic-225157404_50377932
https://vk.com/topic-225157884_50377933
https://vk.com/topic-225157404_50377952
https://vk.com/topic-225157884_50377953
https://vk.com/topic-225157404_50377976
https://vk.com/topic-225157884_50377975

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/